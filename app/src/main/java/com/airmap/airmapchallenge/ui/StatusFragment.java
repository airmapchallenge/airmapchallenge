package com.airmap.airmapchallenge.ui;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airmap.airmapchallenge.R;
import com.airmap.airmapchallenge.api.AirMapApi;
import com.airmap.airmapchallenge.model.Advisory;
import com.airmap.airmapchallenge.model.Status;


public class StatusFragment extends Fragment implements LocationListener {

    private static final String TAG = "StatusFragment";

    private static final int ACCESS_LOCATION_PERMISSION_REQUEST = 0;

    private RecyclerView mAdvisoryRecyclerView;
    private AdvisoryRecyclerAdapter mAdvisoryRecyclerAdapter;

    private Location mLocation;
    private boolean mWeatherEnabled;
    private long mBuffer = 500;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_status, container, false);

        mAdvisoryRecyclerView = (RecyclerView) root.findViewById(R.id.advisories_recycler_view);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdvisoryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        mAdvisoryRecyclerAdapter = new AdvisoryRecyclerAdapter();
        mAdvisoryRecyclerView.setAdapter(mAdvisoryRecyclerAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        getLocation();
    }

    @Override
    public void onPause() {
        super.onPause();

        // stop listening to location updates
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);

        View switchView = MenuItemCompat.getActionView(menu.findItem(R.id.action_weather_switch));
        if (switchView != null) {
            SwitchCompat switchCompat = (SwitchCompat) switchView.findViewById(R.id.toolbar_switch);
            switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                    mWeatherEnabled = checked;
                    Toast.makeText(getActivity(), mWeatherEnabled ? "Weather Enabled" : "Weather Disabled",
                            Toast.LENGTH_SHORT).show();
                    getStatusUpdate();
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_weather_switch: {
                return true;
            }
            case R.id.action_buffer: {
                showBufferDialog();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(menuItem);
            }
        }
    }

    private void getLocation() {
        // check permissions first
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }

        // use location manager for location query and updates (could use Network Provider for less accurate location)
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        // single location request (could use requestLocationUpdates for query location periodically or when user moves)
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ? Criteria.ACCURACY_FINE : Criteria.ACCURACY_COARSE);
            locationManager.requestSingleUpdate(criteria, this, null);
        } else {
            mLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            // if gps, network providers not enabled and no known last location
            // take user to settings to enable
            if (mLocation == null) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                alertDialogBuilder
                        .setTitle("Enable Location")
                        .setMessage("Your location services need to be turned on to use this.")
                        .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                        .create();
                alertDialogBuilder.show();
            }
        }
    }

    private void getStatusUpdate() {
        if (mLocation == null) {
            Log.e(TAG, "No location set");
            return;
        }

        AirMapApi.getStatus(mLocation.getLatitude(), mLocation.getLongitude(),
                mBuffer, mWeatherEnabled, new AirMapApi.ApiCallback<Status>() {
            @Override
            public void onSuccess(Status status) {
                mAdvisoryRecyclerAdapter.setAdvisories(status, status.advisories);
            }

            @Override
            public void onError(Throwable throwable) {
                //TODO: show failure dialog?
            }
        });
    }

    private void showBufferDialog() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_seek_bar, null);

        final TextView valueTextView = (TextView) view.findViewById(R.id.value_text_view);

        final SeekBar seekBar = (SeekBar) view.findViewById(R.id.seek_bar);
        seekBar.setKeyProgressIncrement(50);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                valueTextView.setText("Value: " + i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(view)
                .setTitle("Buffer")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mBuffer = seekBar.getProgress();
                        getStatusUpdate();
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .create();
        alertDialogBuilder.show();
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                ACCESS_LOCATION_PERMISSION_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case ACCESS_LOCATION_PERMISSION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted
                    getLocation();
                } else {
                    // permission denied, show dialog?
                }
                break;
            }
        }
    }

    public void onLocationChanged(Location location) {
        Log.i(TAG, "onLocationChanged: " + location.getLatitude() + ", " + location.getLongitude());

        mLocation = location;
        getStatusUpdate();
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i(TAG, "onStatusChanged: " + provider + " status: " + status);
    }

    public void onProviderEnabled(String provider) {
        Log.i(TAG, "onProviderEnabled: " + provider);
    }

    public void onProviderDisabled(String provider) {
        Log.i(TAG, "onProviderEnabled: " + provider);
    }

    class AdvisoryRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int HEADER_VIEW_TYPE = 0;
        private static final int ITEM_VIEW_TYPE = 1;

        private Status status;
        private Advisory[] advisories;

        public void setAdvisories(Status status, Advisory[] advisories) {
            this.status = status;
            this.advisories = advisories;
            notifyDataSetChanged();
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return HEADER_VIEW_TYPE;
            }

            return ITEM_VIEW_TYPE;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case HEADER_VIEW_TYPE: {
                    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header, parent, false);
                    return new HeaderViewHolder(view);
                }
                case ITEM_VIEW_TYPE:
                default: {
                    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_advisory, parent, false);
                    return new AdvisoryViewHolder(view);
                }
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            if (position == 0) {
                HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
                int dotDrawableRes;
                switch (status.advisory_color) {
                    case "green": {
                        dotDrawableRes = R.drawable.ic_green_dot;
                        break;
                    }
                    case "yellow": {
                        dotDrawableRes = R.drawable.ic_yellow_dot;
                        break;
                    }
                    case "red":
                    default: {
                        dotDrawableRes = R.drawable.ic_red_dot;
                        break;
                    }
                }

                holder.colorImageView.setImageResource(dotDrawableRes);
                holder.locationTextView.setText("Location: " + mLocation.getLatitude() + ", " + mLocation.getLongitude());
                holder.nameTextView.setText("Max Safe Distance: " + status.max_safe_distance);
                holder.weatherTextView.setText("Weather: " + (status.weather != null ? status.weather.toString() : "Unknown"));

            } else {
                AdvisoryViewHolder holder = (AdvisoryViewHolder) viewHolder;
                Advisory advisory = advisories[position - 1];

                int dotDrawableRes;
                switch (advisory.color) {
                    case "green": {
                        dotDrawableRes = R.drawable.ic_green_dot;
                        break;
                    }
                    case "yellow": {
                        dotDrawableRes = R.drawable.ic_yellow_dot;
                        break;
                    }
                    case "red":
                    default: {
                        dotDrawableRes = R.drawable.ic_red_dot;
                        break;
                    }
                }

                holder.colorImageView.setImageResource(dotDrawableRes);
                holder.nameTextView.setText(advisory.name);
                holder.locationTextView.setText(advisory.city + ", " + advisory.state);
                holder.phoneTextView.setText("Type: " + advisory.type);
            }
        }

        @Override
        public int getItemCount() {
            return advisories != null ? advisories.length : 0;
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        public ImageView colorImageView;
        public TextView nameTextView;
        public TextView locationTextView;
        public TextView weatherTextView;

        public HeaderViewHolder(View itemView) {
            super(itemView);

            colorImageView = (ImageView) itemView.findViewById(R.id.dot_image_view);
            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            locationTextView = (TextView) itemView.findViewById(R.id.location_text_view);
            weatherTextView = (TextView) itemView.findViewById(R.id.weather_text_view);
        }
    }

    class AdvisoryViewHolder extends RecyclerView.ViewHolder {

        public ImageView colorImageView;
        public TextView nameTextView;
        public TextView locationTextView;
        public TextView phoneTextView;

        public AdvisoryViewHolder(View itemView) {
            super(itemView);

            colorImageView = (ImageView) itemView.findViewById(R.id.dot_image_view);
            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            locationTextView = (TextView) itemView.findViewById(R.id.location_text_view);
            phoneTextView = (TextView) itemView.findViewById(R.id.phone_text_view);
        }
    }
}

