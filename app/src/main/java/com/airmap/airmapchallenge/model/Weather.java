package com.airmap.airmapchallenge.model;

/**
 * Created by collin on 9/13/16.
 */
public class Weather {

    public String condition;
    public double humidity;
    public double visibility;
    public double precipitation;
    public double temperature;

    public String toString() {
        return temperature + "° " + condition + ", Precip: " +
                (precipitation * 100) + "% Humidity: " + (humidity * 100) + "%";
    }
}
