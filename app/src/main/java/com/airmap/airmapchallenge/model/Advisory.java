package com.airmap.airmapchallenge.model;


/**
 * Created by collin on 9/13/16.
 */
public class Advisory {

    public String id;
    public String name;
    public String last_updated;
    public double latitude;
    public double longitude;
    public double distance;
    public String type;
    public String city;
    public String state;
    public String country;
    public String color;
}
