package com.airmap.airmapchallenge.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by collin on 9/13/16.
 */
public class Status{

    public long max_safe_distance;
    public String advisory_color;
    public Advisory[] advisories;
    public Weather weather;
}
