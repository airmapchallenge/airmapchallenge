package com.airmap.airmapchallenge.model;

/**
 * Created by collin on 9/13/16.
 */
public class ApiResponse<T> {
    public String status;
    public T data;
}
