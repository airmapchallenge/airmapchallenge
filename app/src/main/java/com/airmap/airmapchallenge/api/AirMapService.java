package com.airmap.airmapchallenge.api;

import com.airmap.airmapchallenge.model.ApiResponse;
import com.airmap.airmapchallenge.model.Status;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by collin on 9/13/16.
 */
public interface AirMapService {
    @GET("/status/v2/point")
    void getStatus(@Query("latitude") double latitude, @Query("longitude") double longitude,
                   @Query("buffer") long buffer, @Query("weather") boolean weather, Callback<ApiResponse<Status>> callback);
}
