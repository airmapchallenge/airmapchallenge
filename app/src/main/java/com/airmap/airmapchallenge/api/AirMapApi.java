package com.airmap.airmapchallenge.api;


import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.airmap.airmapchallenge.model.ApiResponse;
import com.airmap.airmapchallenge.model.Status;

import java.util.UUID;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by collin on 9/13/16.
 */
public class AirMapApi {

    private static final String TAG = "AirMapApi";

    private static final String API_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVkZW50aWFsX2lkIjoiY3JlZGVudGlhbHx3WldrNEJKdThZOEF5UWZMZUFSUkJIUTZ5TXlQIiwiYXBwbGljYXRpb25faWQiOiJhcHBsaWNhdGlvbnxZdm9hRW1kaVhNTzBuRGNlMmdPb291ZWRMR0dFIiwib3JnYW5pemF0aW9uX2lkIjoiZGV2ZWxvcGVyfDJ6b2JiN3loeGVZNHFrQzNQUngwWkhLTXoyMzgiLCJpYXQiOjE0NzM2OTU3MDV9.Srlq88aqMIhS_nccl-cxUfNuuA_6Ie_t-u8oRBlHckQ";

    private static final String AIR_MAP_API_BASE = "https://api.airmap.com/";

    /**
     *  Gets a UUID for the device.
     *  Note: this id may not be unique and will change if phone is factory reset
     *  Note: consider using telephony manager (requires extra permission)
     *  Note: or Google's AdvertisingId if this is a problem
     *
     *  @return unique identifier
     */
    public static String getUUID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static void getStatus(final double latitude, final double longitude,
                                 final long buffer, final boolean weather, final ApiCallback<Status> callback) {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(AIR_MAP_API_BASE)
                .setLogLevel(RestAdapter.LogLevel.NONE)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("X-API-Key", API_KEY);
                    }
                })
                .build();

        AirMapService service = restAdapter.create(AirMapService.class);
        service.getStatus(latitude, longitude, buffer, weather, new Callback<ApiResponse<Status>>() {
            @Override
            public void success(ApiResponse<Status> apiResponse, Response response) {
                Status status = apiResponse.data;
                callback.onSuccess(status);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, "Get status failed" , error);
                callback.onError(error);
            }
        });
    }

    public interface ApiCallback<T> {
        void onSuccess(T response);
        void onError(Throwable throwable);
    }
}
